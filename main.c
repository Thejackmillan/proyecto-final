#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <ctype.h>
// Cosas que no hay que modificar
#define ERROR_MINA_ENCONTRADA 1
#define ERROR_ESPACIO_YA_DESCUBIERTO 2
#define ERROR_NINGUNO 3
// Cosas que puedo modificar
#define COLUMNAS 10
#define FILAS 10
#define ESPACIO_SIN_DESCUBRIR '.'
#define ESPACIO_DESCUBIERTO ' '
#define MINA 'M'
#define CANTIDAD_MINAS \
10
#define DEBUG 0 // Cosas del Buscaminas

int main (void)
{
int opc=0;
do{
system("cls");
printf("  **MENU DE OPCIONES**\n\n");
printf("Elije el juego que quieres jugar...\n");
printf("1. El ahorcado \n");
printf("2. Sopa de Letras \n");
printf("3. Busca-minas \n");
printf("4. Salir \n");
printf("Proximamente mas juegos...\n\n");
printf("Digite la opcion deseada: ");
scanf("%d",&opc);
switch(opc){
                                                                    case 1:
system("cls");
{
char frase[60],rep[100],temporal[100],pista[50];
char pal;
int longitud,i,j,inicial,acertado=0,temp=0,oportunidades=5;
int repetido=0,gano=0,opci=0;
do{
system("cls");
printf("1- Jugar\n");
printf("2- Instrucciones\n");
printf("3- Salir\n");
do{scanf("%i", & opci);}while(opci<0||opci>3);
switch(opci){
case 1:
system("cls");
printf("\tJuego del Ahorcado\n");
printf("\n");
gets(pista);
printf("Ingrese la Palabra a adivinar: \n");
gets(frase);
printf("\nIngrese una pista para su companero (por ejemplo: pelicula, serie, color, etc.)\n");
gets(pista);
system("cls");
longitud = 0;
inicial = 0;
j = 0;
rep[0] = ' ';
rep[1] = '\0';
do {
system("cls");
temp=0;
if(inicial == 0) {
for(i=0;i<strlen(frase);i++) {
if(frase[i] == ' ') {
temporal[i] = ' ';
longitud++;
}
else {
temporal[i] = '_';
longitud++;
}
}
}
inicial = 1;
temporal[longitud] = '\0';
for(i=0;i<strlen(rep);i++) {
if(rep[i] == pal) {
repetido = 1;
break;
}
else {
repetido = 0;
}
}
if(repetido == 0) {
for(i=0;i<strlen(frase);i++) {
if(frase[i] == pal) {
temporal[i] = pal;
acertado++;
temp=1;
}
}
}
if(repetido == 0) {
if(temp == 0) {
oportunidades = oportunidades - 1;
}
}
else {
printf("Ya se ha introducido este caracter");
printf("\n\n");
}
printf("\n");
for(i=0;i<strlen(temporal);i++) {
printf(" %c ",temporal[i]);
}
printf("\n");
if(strcmp(frase,temporal) == 0) {
gano = 1;
break;
}
printf("\n");
printf("Letras Acertadas: %d",acertado);
printf("\n");
printf("Oportunidades Restantes: %d",oportunidades);
printf("\n");
rep[j] = pal;
j++;
if (oportunidades==0)
{
break;
}
printf("Tu oponente te dio la siguiente pista: "); puts(pista);
printf("Introduzca una letra:");
scanf("\n%c",&pal);
}while(oportunidades != 0);
if(gano) {
printf("\n\n");
printf("Enhorabuena, has ganado.");
}
else {
printf("\n\n");
printf("Has perdido.");
}
printf("\n\n");
system ("pause");
break;
case 2:
system("cls");
printf ("instrucciones\n");
printf ("\n1. El Juego conciste en dos personas un retador y el retado.");
printf ("\n2. El retador debe escribir la palabra para adivinar y luego escribir una pista para ayudarlo.");
printf ("\n3. El jugador tendra un limite de errores posteriormente este perdera si no adivina la palabra.");
printf ("\n4. El Jugador solo debera escribir letras y presionar enter en caso de conocer la palabra completa puede ponerla.");
printf ("\n5. Es importante diferenciar entre Mayusculas y Minusculas ya que ambas pueden ser usadas.\n");
system("pause");
break;}}
while(opci!=3);
}
break;
                                                            case 2:
system("cls");
{
int op, puntos, oportunidades;
int palabra1=0,palabra2=0,palabra3=0,palabra4=0;
char s[20];
do{
system("cls");
printf("1- Jugar\n");
printf("2- Instrucciones\n");
printf("3- Salir\n");
do{scanf("%i", & op);}while(op<0||op>3);
switch(op){
case 1:
oportunidades=4;
puntos=0;
palabra1=0;palabra2=0;palabra3=0;palabra4=0;
do{
system("cls");
printf("Puntaje           : %i\n", puntos);
printf("Intentos restantes: %i\n", oportunidades);
printf("\nt a t u o m b m i k n a k m i b k n g a\n");
printf("\nh a j f n b r j b s j m e n y b m i n u\n");
printf("\ng h x f s v t e w b y h j r f r d s f u\n");
printf("\nl r f i d v h y f d r w f b o j u b m o\n");
printf("\n j n j m l j l a u t o b u s u n l g b\n");
printf("\ni u b e s t a c i o n a m i e n t o u u\n");
printf("\nl r f s d v h y f d r w f b h j u b m l\n");
printf("\ne a t r e u p o r e a j t r w v k b k j\n");
printf("\ni u b d t e s v l m g o i e a w n i i u\n");
printf("\n\nPalabra:");
scanf("%s", & s);
if(strcmp(s, "estacionamiento")==0&&palabra1==0){
printf("Palabra encontrada! 5 puntos\n");
puntos=puntos+5;
oportunidades=oportunidades-1;
palabra1=1;
system("pause");
}else if(strcmp(s, "autobus")==0&&palabra2==0){
printf("Palabra encontrada! 5 puntos\n");
puntos=puntos+5;
oportunidades=oportunidades-1;
palabra2=1;
system("pause");
}else if(strcmp(s, "taxi")==0&&palabra3==0){
printf("Palabra encontrada! 5 puntos\n");
puntos=puntos+5;
oportunidades=oportunidades-1;
palabra3=1;}
else if(strcmp(s, "puerta")==0&&palabra4==0){
printf("Palabra encontrada! 5 puntos\n");
puntos=puntos+5;
oportunidades=oportunidades-1;
palabra4=1;
}else{
printf("\n\nIncorrecto.\n");
oportunidades=oportunidades-1;
system("pause");}}
while(oportunidades!=0);
system("cls");
printf("Juego acabado!\n");
printf("\nTus puntos: %i\n", puntos);
system("pause");
break;
case 2:
system("cls");
printf ("instrucciones\n");
printf ("\n1. Decifre las palabras escondidas entre las letras.");
printf ("\n2. Haga una lista introduciendo las palabras que encontro, el mismo programa resolvera sus asiertos y sus errores.");
printf ("\n3. Por cada asierto se sumaran puntos y por cada error se restarn los mismos.");
printf ("\n4. Cuando se acaben los intentos , el juego finalizara y obtendra el puntaje final.\n");
system("pause");
break;}}
while(op!=3);
}
break;
                                                                case 3:
system("cls");
int opcio=0;
do{
system("cls");
printf("1- Jugar\n");
printf("2- Instrucciones\n");
printf("3- Salir\n");
do{scanf("%i", & opcio);}while(opcio<0||opcio>3);
switch(opcio){
case 1:
system("cls");
int obtenerMinasCercanas(int fila, int columna, char tablero[FILAS][COLUMNAS]) {
int conteo = 0, filaInicio, filaFin, columnaInicio, columnaFin;
if (fila <= 0) {
filaInicio = 0;
} else {
filaInicio = fila - 1;
}
if (fila + 1 >= FILAS) {
filaFin = FILAS - 1;
} else {
filaFin = fila + 1;
}
if (columna <= 0) {
columnaInicio = 0;
} else {
columnaInicio = columna - 1;
}
if (columna + 1 >= COLUMNAS) {
columnaFin = COLUMNAS - 1;
} else {
columnaFin = columna + 1;
}
int m;
for (m = filaInicio; m <= filaFin; m++) {
int l;
for (l = columnaInicio; l <= columnaFin; l++) {
if (tablero[m][l] == MINA) {
conteo++;
}
}
}
return conteo;
}
int aleatorioEnRango(int minimo, int maximo) {
return minimo + rand() / (RAND_MAX / (maximo - minimo + 1) + 1);
}
void iniciarTablero(char tablero[FILAS][COLUMNAS]) {
int l;
for (l = 0; l < FILAS; l++) {
int m;
for (m = 0; m < COLUMNAS; m++) {
tablero[l][m] = ESPACIO_SIN_DESCUBRIR;
}
}
}
void colocarMina(int fila, int columna, char tablero[FILAS][COLUMNAS]) {
tablero[fila][columna] = MINA;
}
void colocarMinasAleatoriamente(char tablero[FILAS][COLUMNAS]) {
int l;
for (l = 0; l < CANTIDAD_MINAS; l++) {
int fila = aleatorioEnRango(0, FILAS - 1);
int columna = aleatorioEnRango(0, COLUMNAS - 1);
colocarMina(fila, columna, tablero);
}
}
void imprimirSeparadorEncabezado() {
int m;
for (m = 0; m <= COLUMNAS; m++) {
printf("----");
if (m + 2 == COLUMNAS) {
printf("-");
}
}
printf("\n");
}
void imprimirSeparadorFilas() {
int m;
for (m = 0; m <= COLUMNAS; m++) {
printf("+---");
if (m == COLUMNAS) {
printf("+");
}
}
printf("\n");
}
void imprimirEncabezado() {
imprimirSeparadorEncabezado();
printf("|   ");
int l;
for (l = 0; l < COLUMNAS; l++) {
printf("| %d ", l + 1);
if (l + 1 == COLUMNAS) {
printf("|");
}
}
printf("\n");
}
char enteroACaracter(int numero) {
return numero + '0';
}
void imprimirTablero(char tablero[FILAS][COLUMNAS], int deberiaMostrarMinas) {
imprimirEncabezado();
imprimirSeparadorEncabezado();
char letra = 'A';
int l;
for (l = 0; l < FILAS; l++) {
int m;
printf("| %c ", letra);
letra++;
for (m = 0; m < COLUMNAS; m++) {
char verdaderaLetra = ESPACIO_SIN_DESCUBRIR;
char letraActual = tablero[l][m];
if (letraActual == MINA) {
verdaderaLetra = ESPACIO_SIN_DESCUBRIR;
} else if (letraActual == ESPACIO_DESCUBIERTO) {
int minasCercanas = obtenerMinasCercanas(l, m, tablero);
verdaderaLetra = enteroACaracter(minasCercanas);
}
if (letraActual == MINA && (DEBUG || deberiaMostrarMinas)) {
verdaderaLetra = MINA;
}
printf("| %c ", verdaderaLetra);
if (m + 1 == COLUMNAS) {
printf("|");
}
}
printf("\n");
imprimirSeparadorFilas();
}
}
int abrirCasilla(char filaLetra, int columna, char tablero[FILAS][COLUMNAS]) {
filaLetra = toupper(filaLetra);
columna--;
int fila = filaLetra - 'A';
assert(columna < COLUMNAS && columna >= 0);
assert(fila < FILAS && fila >= 0);
if (tablero[fila][columna] == MINA) {
return ERROR_MINA_ENCONTRADA;
}
if (tablero[fila][columna] == ESPACIO_DESCUBIERTO) {
return ERROR_ESPACIO_YA_DESCUBIERTO;
}
tablero[fila][columna] = ESPACIO_DESCUBIERTO;
return ERROR_NINGUNO;
}
int noHayCasillasSinAbrir(char tablero[FILAS][COLUMNAS]) {
int l;
for (l = 0; l < FILAS; l++) {
int m;
for (m = 0; m < COLUMNAS; m++) {
char actual = tablero[l][m];
if (actual == ESPACIO_SIN_DESCUBRIR) {
return 0;
}
}
}
return 1;
}
{
printf("** BUSCAMINAS **\n");
char tablero[FILAS][COLUMNAS];
int deberiaMostrarMinas = 0;
srand(getpid());
iniciarTablero(tablero);
colocarMinasAleatoriamente(tablero);
while (1) {
imprimirTablero(tablero, deberiaMostrarMinas);
if (deberiaMostrarMinas) {
break;
}
int columna;
char fila;
printf("Ingresa la fila: ");
scanf(" %c", &fila);
printf("Ingresa la columna: ");
scanf("%d", &columna);
int status = abrirCasilla(fila, columna, tablero);
if (noHayCasillasSinAbrir(tablero)) {
printf("Has ganado\n");
deberiaMostrarMinas = 1;
} else if (status == ERROR_ESPACIO_YA_DESCUBIERTO) {
printf("Ya has abierto esta casilla\n");
} else if (status == ERROR_MINA_ENCONTRADA) {
printf("Has perdido\n");
deberiaMostrarMinas = 1;
}
}
}
break;
case 2:
system("cls");
printf ("instrucciones\n");
printf ("\n1. Escriba la fila en la que cree que no hay mina.");
printf ("\n2. Escriba la columna en la que cree que no hay mina.");
printf ("\n3. Debe tratar de evitar las minas que hay escondidas.");
printf ("\n4. Es importante que escriba primero la fila presione Enter y luego la columna y Enter nuevamente.");
printf ("\n5. Cuando termine de llenar las casillas sin encontrar Minas habra ganado de lo contrario sera una derrota.\n");
system("pause");
break;}}
while(opcio!=3);
break;
case 4:
break;
default:
system("cls");
printf("La opcion que ingreso es incorrecta por favor intentelo de nuevo, gracias.");
getch ();
break;
}
}while(opc !=4);
return 0;
}
